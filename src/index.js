import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import initializeDb from './db';
import middleware from './middleware';
import config from './config.json';

var dateFormat = require('dateformat');
var moment = require('moment');
var now = new Date();
var fs = require('fs');

let app = express();
app.server = http.createServer(app);

// logger
app.use(morgan('dev'));

// 3rd party middleware
app.use(cors({
	exposedHeaders: config.corsHeaders
}));

app.use(bodyParser.json({
	limit : config.bodyLimit
}));

// connect to db
initializeDb( db => {

	// internal middleware
	app.use(middleware({ config, db }));

	// api router
	app.get('/', (req, res) => {
		var result = [];
		var data;
		try{
			data = fs.readFileSync('./src/logs/customer-visit.log', 'utf8');
		}catch(err){
			if (err.code === 'ENOENT') {
			  res.json(err.code);
			} else {
			  throw err;
			}
		}
			var lines = data.split('\r\n');
		    var customerVisit = [];
		    lines.map(function(item){
		    	var customer  = item.split('\t');
		    	var visitTime = dateFormat(customer[0], "dd-mm-yyyy");
		    	var customerID = customer[1];
		    	if(!(customerID in customerVisit)){
		    		customerVisit[customerID] = [];
		    	}	
		    	customerVisit[customerID].push(visitTime);
		    });
		    for(var key in customerVisit) {
		    	var count = 0;
			    if(customerVisit.hasOwnProperty(key)){
			    	var visits = customerVisit[key];
			    	visits.map(function(visit,index){
			    		if(index + 1 in visits){
				    		var startDate = moment(visit, "DD-MM-YYYY");
							var endDate = moment(visits[index + 1], "DD-MM-YYYY");
							var diff = endDate.diff(startDate, 'days');
				    		if(diff == 1){
				    			count++;
				    		}
				    	}
			    	});
			    }
			    if(count >= 2){
		    		result.push(key);
		    	}
			}
		
		res.json({ result });

	});

	app.server.listen(process.env.PORT || config.port, () => {
		console.log(`Started on port ${app.server.address().port}`);
	});
});

export default app;
