# README #

Getting Started
---------------
# Install dependencies 
npm install

# Start development live-reload server
PORT=8080 npm run dev

# Start production server:
PORT=8080 npm start


# Problem #
---------------
Provide several examples that call your function and demonstrate that it works.

Provide the average runtime and space complexity (memory usage), and worst-case runtime
and space complexity for your solution, and a short explanation as to why.
State any assumptions you make for your solution.

Question:
Given an access log for a feature, output the customer ids of repeat customers who have visited on 3 or
more consecutive days. Each line of the access log is tab delimited with two fields: the timestamp of
when the customer visited, and the customer id (a 10 byte string). The feature writes an entry to the log
file as it gets the hits. Below is an example log file.

08-Jun-2012 1:00 AM 4ABCDEFGHI

09-Jun-2012 1:00 AM 1ABCDEFGHI

09-Jun-2012 9:23 AM 3ABCDEFGHI

10-Jun-2012 1:00 AM 2ABCDEFGHI

10-Jun-2012 2:03 AM 2ABCDEFGHI

10-Jun-2012 1:00 AM 1ABCDEFGHI

10-Jun-2012 7:23 AM 3ABCDEFGHI

10-Jun-2012 9:23 AM 3ABCDEFGHI

11-Jun-2012 1:00 AM 1ABCDEFGHI

11-Jun-2012 2:12 AM 2ABCDEFGHI

11-Jun-2012 8:23 AM 3ABCDEFGHI

12-Jun-2012 10:21 PM 1ABCDEFGHI

In this example, the 3-consecutive-day repeat customers are "1ABCDEFGHI" and "3ABCDEFGHI". The
result that your program generates will be these two customer ids.


# Solution #
---------------
Step 1: Create array with customer ID as key and push array of visittime for that customer as value

Step 2: Find difference between 2 adjacent visit-dates for each customer and count if its 1 day difference

Step 3: Print id of customers for which count is 3 or more

# Examples #

Input
---------------
08-Jun-2012 1:00 AM	4ABCDEFGHI

09-Jun-2012 1:00 AM	1ABCDEFGHI

09-Jun-2012 9:23 AM	3ABCDEFGHI

10-Jun-2012 1:00 AM	2ABCDEFGHI

10-Jun-2012 2:03 AM	2ABCDEFGHI

10-Jun-2012 1:00 AM	1ABCDEFGHI

10-Jun-2012 7:23 AM	3ABCDEFGHI

10-Jun-2012 9:23 AM	3ABCDEFGHI

11-Jun-2012 1:00 AM	1ABCDEFGHI

11-Jun-2012 2:12 AM	2ABCDEFGHI

11-Jun-2012 8:23 AM	3ABCDEFGHI

12-Jun-2012 10:21 PM 1ABCDEFGHI

12-Jun-2012 1:00 AM	2ABCDEFGHI

Output
---------------
"1ABCDEFGHI",
"3ABCDEFGHI",
"2ABCDEFGHI"

---------------

Input
---------------
08-Jun-2012 1:00 AM	4ABCDEFGHI

09-Jun-2012 1:00 AM	1ABCDEFGHI

09-Jun-2012 9:23 AM	3ABCDEFGHI

09-Jun-2012 1:00 AM	4ABCDEFGHI

10-Jun-2012 1:00 AM	2ABCDEFGHI

10-Jun-2012 2:03 AM	2ABCDEFGHI

10-Jun-2012 1:00 AM	1ABCDEFGHI

10-Jun-2012 7:23 AM	3ABCDEFGHI

10-Jun-2012 9:23 AM	3ABCDEFGHI

10-Jun-2012 1:00 AM	4ABCDEFGHI

11-Jun-2012 1:00 AM	1ABCDEFGHI

11-Jun-2012 2:12 AM	2ABCDEFGHI

11-Jun-2012 8:23 AM	3ABCDEFGHI

12-Jun-2012 10:21 PM 1ABCDEFGHI

12-Jun-2012 1:00 AM	2ABCDEFGHI

Output
---------------
"4ABCDEFGHI",
"1ABCDEFGHI",
"3ABCDEFGHI",
"2ABCDEFGHI"